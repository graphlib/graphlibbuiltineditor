/*

Shader for flattening layers 
input is layers flattened out in [l1..,ln]
output will be [l1->lcount,ln -> lcount...] with the layer 
expression applied at each iteration

we have the layer count
each layer needs to know its operation
second buffer is the ops to do in each layer

for each  layer starting at layer 1(0 is background layer)
fetch layer[n] and layer [n + 1]
fetch op to apply 0 is a normal op 
1 is mul etc..

write result to output buffer texture

*/

RWBuffer<float4> layer_data : register( u0 );
RWBuffer<uint> layer_ops_data : register( u1 );

RWTexture2D<float4> flatten_result_texture : register(u2);

struct IndexedStruct{
	int layer_count;
};

StructuredBuffer<IndexedStruct> index : register(t1);

[numthreads(1, 1, 1)]
void main(uint3 gid : SV_GroupID,uint3 DTid : SV_DispatchThreadID,uint gi : SV_GroupIndex){
	int layer_count = index[gid.x].layer_count;
	for (int i = 0;i < layer_count;i+=1){
		float4 texel_color = layer_data[gid.x + i];
		uint tex_op = layer_ops_data[gid.x + i];
		texel_color *= 2;
		layer_data[gid.x + i] = texel_color;
		//later we may need to split each op into its own shader to help with 
		//minimizing divergence if that ends up a problem for now we are doing 
		//the naive thing
		if (tex_op == 0){
			//normal blend the layers
			flatten_result_texture[DTid.xy + i] = float4(0,0,1,1);
		}else if(tex_op == 1){
			//multiply blend the layers
			flatten_result_texture[DTid.xy + i] = float4(0,0,1,1);
		}
		//etc..
	}

	GroupMemoryBarrierWithGroupSync();
}

