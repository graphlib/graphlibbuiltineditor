
struct IndexedStruct{
	uint index;
	uint mettalic_rougness_tex_index;
	uint normal_tex_index;
	uint emmisive_tex_index;
	float4 base_color;
};

struct PixelShaderInput
{
    float4 Position : SV_Position;
    float4 Color : COLOR;
    float2 UV : TEXCOORD0;
};

Texture2D ts[1000] : register(t10);
SamplerState s1 : register(s0);
ConstantBuffer<IndexedStruct> index : register(b12);

float4 main( PixelShaderInput IN ) : SV_Target0{
	float4 color = ts[index.index].Sample(s1, IN.UV);
	return color;
}

