struct VertexShaderOutput
{
    float4 position : SV_Position;    
    float4 Color : COLOR;
    float2 UV : TEXCOORD0;
};

VertexShaderOutput main(in VertexData vert_in)
{
	VertexShaderOutput OUT;
	OUT.Color = float4(1,1,1,1);
	OUT.UV = float2(1,1);
	OUT.position = float4(1,1,1,1);

	//TODO(Ray):Right now we are saying the projection matrix is always 0 
	//later we will get this from cameras that have a custom setting and 
	//set this to that through the PerDrawConstants
	float4x4 projection = matrix_buffer[0];
	float4x4 model = matrix_buffer[index.model_mat_index];
	float4x4 world = matrix_buffer[index.world_mat_index];
	float4x4 mod_to_world = mul(world,model);
	float4x4 clip = mul(projection, mod_to_world);
	//float4x4 m = matrix_buffer[0];

	//float4 p = mul(float4(vert_in.Position,1.0f),clip);
	float4 p = mul(clip,float4(vert_in.Position,1.0f));

	//float4 world_p = mul(float4(vert_in.Position,1.0f),mm);

	OUT.position = p;
//	OUT.frag_p = world_p;    
	float4 fake_light_p = float4(0,0,0,1);

	OUT.Color = vert_in.Color;//(mul(fake_light_p,mm));//float4(1,1,1,1);
	OUT.UV = vert_in.UV;
	//float3 n = vert_in.normal;

	//OUT.normal = vert_in.normal;///mul(n,);

	return OUT;
}
