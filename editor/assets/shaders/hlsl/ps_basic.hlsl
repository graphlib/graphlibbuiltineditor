float4x4 inverse(float4x4 m) {
    float n11 = m[0][0], n12 = m[1][0], n13 = m[2][0], n14 = m[3][0];
    float n21 = m[0][1], n22 = m[1][1], n23 = m[2][1], n24 = m[3][1];
    float n31 = m[0][2], n32 = m[1][2], n33 = m[2][2], n34 = m[3][2];
    float n41 = m[0][3], n42 = m[1][3], n43 = m[2][3], n44 = m[3][3];

    float t11 = n23 * n34 * n42 - n24 * n33 * n42 + n24 * n32 * n43 - n22 * n34 * n43 - n23 * n32 * n44 + n22 * n33 * n44;
    float t12 = n14 * n33 * n42 - n13 * n34 * n42 - n14 * n32 * n43 + n12 * n34 * n43 + n13 * n32 * n44 - n12 * n33 * n44;
    float t13 = n13 * n24 * n42 - n14 * n23 * n42 + n14 * n22 * n43 - n12 * n24 * n43 - n13 * n22 * n44 + n12 * n23 * n44;
    float t14 = n14 * n23 * n32 - n13 * n24 * n32 - n14 * n22 * n33 + n12 * n24 * n33 + n13 * n22 * n34 - n12 * n23 * n34;

    float det = n11 * t11 + n21 * t12 + n31 * t13 + n41 * t14;
    float idet = 1.0f / det;

    float4x4 ret;

    ret[0][0] = t11 * idet;
    ret[0][1] = (n24 * n33 * n41 - n23 * n34 * n41 - n24 * n31 * n43 + n21 * n34 * n43 + n23 * n31 * n44 - n21 * n33 * n44) * idet;
    ret[0][2] = (n22 * n34 * n41 - n24 * n32 * n41 + n24 * n31 * n42 - n21 * n34 * n42 - n22 * n31 * n44 + n21 * n32 * n44) * idet;
    ret[0][3] = (n23 * n32 * n41 - n22 * n33 * n41 - n23 * n31 * n42 + n21 * n33 * n42 + n22 * n31 * n43 - n21 * n32 * n43) * idet;

    ret[1][0] = t12 * idet;
    ret[1][1] = (n13 * n34 * n41 - n14 * n33 * n41 + n14 * n31 * n43 - n11 * n34 * n43 - n13 * n31 * n44 + n11 * n33 * n44) * idet;
    ret[1][2] = (n14 * n32 * n41 - n12 * n34 * n41 - n14 * n31 * n42 + n11 * n34 * n42 + n12 * n31 * n44 - n11 * n32 * n44) * idet;
    ret[1][3] = (n12 * n33 * n41 - n13 * n32 * n41 + n13 * n31 * n42 - n11 * n33 * n42 - n12 * n31 * n43 + n11 * n32 * n43) * idet;

    ret[2][0] = t13 * idet;
    ret[2][1] = (n14 * n23 * n41 - n13 * n24 * n41 - n14 * n21 * n43 + n11 * n24 * n43 + n13 * n21 * n44 - n11 * n23 * n44) * idet;
    ret[2][2] = (n12 * n24 * n41 - n14 * n22 * n41 + n14 * n21 * n42 - n11 * n24 * n42 - n12 * n21 * n44 + n11 * n22 * n44) * idet;
    ret[2][3] = (n13 * n22 * n41 - n12 * n23 * n41 - n13 * n21 * n42 + n11 * n23 * n42 + n12 * n21 * n43 - n11 * n22 * n43) * idet;

    ret[3][0] = t14 * idet;
    ret[3][1] = (n13 * n24 * n31 - n14 * n23 * n31 + n14 * n21 * n33 - n11 * n24 * n33 - n13 * n21 * n34 + n11 * n23 * n34) * idet;
    ret[3][2] = (n14 * n22 * n31 - n12 * n24 * n31 - n14 * n21 * n32 + n11 * n24 * n32 + n12 * n21 * n34 - n11 * n22 * n34) * idet;
    ret[3][3] = (n12 * n23 * n31 - n13 * n22 * n31 + n13 * n21 * n32 - n11 * n23 * n32 - n12 * n21 * n33 + n11 * n22 * n33) * idet;

    return ret;
}

static const float PI = 3.14159265f;
//PBR
//Fresnel
float3 fresnelSchlick(float cosTheta, float3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

//Geometry Function 
float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = max((r*r) / 8.0,0);

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;
	
    return max(num / denom,0);
}
float GeometrySmith(float3 N, float3 V, float3 L, float roughness)
{
    float NoV = max(dot(N, V), 0.0);
    float NoL = max(dot(N, L), 0.0);
	/*
	float a2 = roughness * roughness;
	float GGXV = NoL * sqrt(NoV * NoV * (1.0 - a2) + a2);
    float GGXL = NoV * sqrt(NoL * NoL * (1.0 - a2) + a2);
    return 0.5 / (GGXV + GGXL);
	*/

   	float ggxv  = GeometrySchlickGGX(NoV, roughness);
    float ggxl  = GeometrySchlickGGX(NoL, roughness);
    return ggxv * ggxl;
}

//NDF: Normal Distribution Function
float DistributionGGX(float3 N, float3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;
	
    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;
	
    return num / denom;
}

//Creates a TBN matrix
float3x3 cotangent_frame( float3 N, float3 p, float2 uv )
{
    // get edge vectors of the pixel triangle
    float3 dp1 = ddx ( p );
    float3 dp2 = ddy ( p );
    float2 duv1 = ddx ( uv );
    float2 duv2 = ddy ( uv );
 
    // solve the linear system
    float3 dp2perp = cross( dp2, N );
    float3 dp1perp = cross( N, dp1 );
    float3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    float3 B = dp2perp * duv1.y + dp1perp * duv2.y;
 
    // construct a scale-invariant frame 
    float invmax = rsqrt( max( dot(T,T), dot(B,B) ) + 0.0001 );
    return transpose(float3x3( T * invmax, B * invmax, N ));
}

struct IndexedStruct{
	uint index;
	uint mettalic_rougness_tex_index;
	uint normal_tex_index;
	uint emmisive_tex_index;
	float4 base_color;
	float metallic;
	float roughness;
	float emmisive;
	float dummy;
};

struct PixelShaderInput
{
    float4 Position : SV_Position;
    float4 Color : COLOR0;
    float2 UV : TEXCOORD0;
	float3 normal : NORMAL;
	float4 frag_p : COLOR1;
	nointerpolation float4 cam_p : CUSTOMPOSITION;
	nointerpolation float world_mat_index : PSIZE;
	nointerpolation float view_projection_index : PSIZE1;
	nointerpolation float dummyo : PSIZE2;
	nointerpolation float dummyt : PSIZE3;
	nointerpolation float4 world_vertex_p : COLOR2;
	
};

struct Light
{
	uint type;
	float4 position;
	float4 color;
	float4 direction;
	float radius;
	float inner_angle_cos;
	float outer_angle_cos;
	float pad;
};

Texture2D ts[1000] : register(t10);
SamplerState s1 : register(s0);
ConstantBuffer<IndexedStruct> index : register(b12);
StructuredBuffer<Light> light_buffer : register(t1);
StructuredBuffer<float4x4> matrix_buffer : register(t0);

float4 main( PixelShaderInput IN ) : SV_Target0{
	float4 color = ts[index.index].Sample(s1, IN.UV);
	//color = lerp(color,index.base_color,(float4(color.aaaa) - 1) * -1);
	if (index.index == 0)
	{
		color = index.base_color;
	}

	float4 metal_rough = ts[index.mettalic_rougness_tex_index].Sample(s1, IN.UV);
	//metal_rough = lerp(metal_rough,0,(float2(metal_rough.bg) - 1) * -1);
	//if (metal_rough.b == 0)
	if (index.mettalic_rougness_tex_index == 0)
	{
		metal_rough = float4(0,0,0,0);
		metal_rough.b = index.metallic;
		metal_rough.g = index.roughness;
	}

	float4 normal_tex = ts[index.normal_tex_index].Sample(s1, IN.UV);

	float3 frag_p = IN.frag_p.xyz;
	float3 world_vertex_p = IN.world_vertex_p.xyz;
	float3 light_p = light_buffer[0].position.xyz;
	float3 light_color = light_buffer[0].color.rgb;
	
	float3 view_dir = normalize(IN.cam_p.xyz - frag_p);
	float3 world_vertex_view_dir = normalize(IN.cam_p.xyz - world_vertex_p);
	//float3 view_dir_vertex = normalize(IN.cam_p.xyz - IN.Position.xyz);
	//float4 emm = ts[index.emmisive_tex_index].Sample(s1, IN.UV);
	float4x4 world_matrix = matrix_buffer[int(IN.world_mat_index)];
	float3 in_normal_world = normalize(mul(world_matrix, float4(IN.normal,0)).xyz);

	float3 n = in_normal_world;

	if (index.normal_tex_index != 0){
		float3x3 TBN = cotangent_frame(in_normal_world,-world_vertex_view_dir,IN.UV);
		//options for certain types of situations off for now
		//normal_tex.y = -normal_tex.y;
		//normal_tex = normal_tex * 255./127. - 128./127.;
		n = normalize(mul(TBN,normal_tex.rgb));
	}

	float3 l = normalize(light_p - frag_p);
	float ln_dot = dot(n,l);
	float3 diffuse = color.rgb * light_color * max(ln_dot,0);
	
	//float cam_ln_dot = dot(n,view_dir);
	//float3 specular = color.rgb * pow(max(cam_ln_dot,0), 64);
	float distance = length(l);
	float attenuation = 1.0 / pow(distance,2);  
	//color.rgb = (diffuse + specular) * attenuation;
	//TODO(Ray):Add gamma correction if needed.
	//TODO(Ray):Add emissive
	//TODO(Ray):Add normal mapping
	//TODO(Ray):Add roughness
	//TODO(Ray):Add mettalic
	//Add ambient lighting
	//Add shadow mapping
	//Add reflection
	//Add refraction
	//Add fog
	//Add bloom
	//Add motion blur
	//Add depth of field
	//Add screen space ambient occlusion
	//Add reflections
	//Add refraction
	//Add  subsurface scattering
	//Add  global illumination
	//Add volumetric lighting
	//Add  volumetric fog
	//Add  volumetric scattering
	//Using PBR terms for lighting
	float3  albedo = color.rgb;
	float3 V = view_dir;
	float3 L = l;
	float3 N = n;
	float3 Lo = 0.0;
	//from here loop over all lights affecting this fragment.
	float metallic = metal_rough.b;
	float roughness = metal_rough.g;
	float ao = 1.0;
	float3 wi = normalize(light_p - frag_p);
	float cosTheta = max(dot(n, wi), 0.0);
	float3 radiance = light_color * attenuation;
    float3 H = normalize(V + L);

	float3 F0 = float3(0.04,0.04,0.04);
	F0 = lerp(F0, albedo, metallic);
	float3 F  = fresnelSchlick(max(dot(H, V), 0.1), F0);
	float NDF = DistributionGGX(N, H, roughness);       
	float G   = GeometrySmith(N, V, L, roughness); 
	float3 numerator    = NDF * G * F;
	float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0)  + 0.0001;
	float3 S = numerator / denominator; 
	float3 kS = F;
	float3 kD = float3(1.0,1.0,1.0) - kS;
	kD *= 1.0 - metallic;
	float NdotL = max(dot(N, L), 0.0);        
    Lo += (kD * albedo / PI + S) * radiance * NdotL;
	float3 ambient = float3(0.13,0.13,0.13) * albedo * ao;
	color = float4(ambient + Lo,1.0);
	//color = float4(metallic,metallic,metallic,1.0);
	//color = float4(roughness,roughness,roughness,1.0);
	//color = float4(normal_tex.rgb,1.0);
	return color;
}

