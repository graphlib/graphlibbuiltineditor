float4x4 inverse(float4x4 m) {
    float n11 = m[0][0], n12 = m[1][0], n13 = m[2][0], n14 = m[3][0];
    float n21 = m[0][1], n22 = m[1][1], n23 = m[2][1], n24 = m[3][1];
    float n31 = m[0][2], n32 = m[1][2], n33 = m[2][2], n34 = m[3][2];
    float n41 = m[0][3], n42 = m[1][3], n43 = m[2][3], n44 = m[3][3];

    float t11 = n23 * n34 * n42 - n24 * n33 * n42 + n24 * n32 * n43 - n22 * n34 * n43 - n23 * n32 * n44 + n22 * n33 * n44;
    float t12 = n14 * n33 * n42 - n13 * n34 * n42 - n14 * n32 * n43 + n12 * n34 * n43 + n13 * n32 * n44 - n12 * n33 * n44;
    float t13 = n13 * n24 * n42 - n14 * n23 * n42 + n14 * n22 * n43 - n12 * n24 * n43 - n13 * n22 * n44 + n12 * n23 * n44;
    float t14 = n14 * n23 * n32 - n13 * n24 * n32 - n14 * n22 * n33 + n12 * n24 * n33 + n13 * n22 * n34 - n12 * n23 * n34;

    float det = n11 * t11 + n21 * t12 + n31 * t13 + n41 * t14;
    float idet = 1.0f / det;

    float4x4 ret;

    ret[0][0] = t11 * idet;
    ret[0][1] = (n24 * n33 * n41 - n23 * n34 * n41 - n24 * n31 * n43 + n21 * n34 * n43 + n23 * n31 * n44 - n21 * n33 * n44) * idet;
    ret[0][2] = (n22 * n34 * n41 - n24 * n32 * n41 + n24 * n31 * n42 - n21 * n34 * n42 - n22 * n31 * n44 + n21 * n32 * n44) * idet;
    ret[0][3] = (n23 * n32 * n41 - n22 * n33 * n41 - n23 * n31 * n42 + n21 * n33 * n42 + n22 * n31 * n43 - n21 * n32 * n43) * idet;

    ret[1][0] = t12 * idet;
    ret[1][1] = (n13 * n34 * n41 - n14 * n33 * n41 + n14 * n31 * n43 - n11 * n34 * n43 - n13 * n31 * n44 + n11 * n33 * n44) * idet;
    ret[1][2] = (n14 * n32 * n41 - n12 * n34 * n41 - n14 * n31 * n42 + n11 * n34 * n42 + n12 * n31 * n44 - n11 * n32 * n44) * idet;
    ret[1][3] = (n12 * n33 * n41 - n13 * n32 * n41 + n13 * n31 * n42 - n11 * n33 * n42 - n12 * n31 * n43 + n11 * n32 * n43) * idet;

    ret[2][0] = t13 * idet;
    ret[2][1] = (n14 * n23 * n41 - n13 * n24 * n41 - n14 * n21 * n43 + n11 * n24 * n43 + n13 * n21 * n44 - n11 * n23 * n44) * idet;
    ret[2][2] = (n12 * n24 * n41 - n14 * n22 * n41 + n14 * n21 * n42 - n11 * n24 * n42 - n12 * n21 * n44 + n11 * n22 * n44) * idet;
    ret[2][3] = (n13 * n22 * n41 - n12 * n23 * n41 - n13 * n21 * n42 + n11 * n23 * n42 + n12 * n21 * n43 - n11 * n22 * n43) * idet;

    ret[3][0] = t14 * idet;
    ret[3][1] = (n13 * n24 * n31 - n14 * n23 * n31 + n14 * n21 * n33 - n11 * n24 * n33 - n13 * n21 * n34 + n11 * n23 * n34) * idet;
    ret[3][2] = (n14 * n22 * n31 - n12 * n24 * n31 - n14 * n21 * n32 + n11 * n24 * n32 + n12 * n21 * n34 - n11 * n22 * n34) * idet;
    ret[3][3] = (n12 * n23 * n31 - n13 * n22 * n31 + n13 * n21 * n32 - n11 * n23 * n32 - n12 * n21 * n33 + n11 * n22 * n33) * idet;

    return ret;
}

struct PerDrawConstants
{
	uint vertexOffset;
    uint vertexBufferIndex;
    uint mat_id;
	uint world_mat_id;
};

struct VertexPosColor
{
    float3 Position : POSITION;
    float3 normal : NORMAL;
    float2 UV : TEXCOORD;
	float4 Color : COLOR;
};

struct VertexShaderOutput
{
    float4 position : SV_Position0;    
    float4 Color : COLOR0;
    float2 UV : TEXCOORD0;
	float3 normal : NORMAL;
    float4 frag_p : COLOR1;
	nointerpolation float4 cam_p : CUSTOMPOSITION;
	nointerpolation float world_mat_index : PSIZE;
	nointerpolation float view_projection_index : PSIZE1;
	nointerpolation float dummyo : PSIZE2;
	nointerpolation float dummyt : PSIZE3;
	nointerpolation float4 world_vertex_p : COLOR2;
};

struct IndexedStruct{
	uint index;
	uint world_mat_index;
	uint projection_mat_index;
	uint position_index;
    uint normal_index;
    uint uv_index;
    uint color_index;
};

ByteAddressBuffer BufferTable[1000] : register(t10);
//StructuredBuffer<VertexPosColor> BufferTable : register(t3,space1);
//ByteAddressBuffer BufferTable[1000] : register(t10);
StructuredBuffer<float4x4> matrix_buffer : register(t0);
ConstantBuffer<PerDrawConstants> draw_constants : register(b0);
ConstantBuffer<IndexedStruct> index : register(b11);

VertexShaderOutput main(uint vertexID : SV_VertexID)
{
	/*
    VertexPosColor vert_in;
    vert_in.Position = float3(1,1,1);
    vert_in.Color = float4(1,1,1,1);
    vert_in.UV = float2(1,1);
    vert_in.normal = float3(1,1,1);
	*/

	VertexShaderOutput OUT;
	OUT.Color = float4(1,1,1,1);
	OUT.UV = float2(1,1);
	OUT.position = float4(1,1,1,1);

	//TODO(Ray):Right now we are saying the projection matrix is always 0 
	//later we will get this from cameras that have a custom setting and 
	//set this to that through the PerDrawConstants
    //uint index_id = BufferTable[22].Load(vertexID * 4);
    float3 vps = asfloat(BufferTable[index.position_index].Load3(vertexID * 12));
    float3 normalps = asfloat(BufferTable[index.normal_index].Load3(vertexID * 12));
    float2 uvps = asfloat(BufferTable[index.uv_index].Load2(vertexID * 8));

	//NOTEI(Ray):Float4x4 types not supported by load in SM6 yet
	float4x4 projection = matrix_buffer[index.projection_mat_index];
	//world
	float4x4 model = matrix_buffer[index.index];
	//camera/viewmmat
	float4x4 world = matrix_buffer[index.world_mat_index];
	
	//TODO(Ray):Some day it would be nice to get this to work.
	/*
	float4x4 projection = BufferTable[2].Load<float4x4>(index.projection_mat_index * sizeof(float4x4));
	float4x4 model = BufferTable[2].Load<float4x4>(index.index * sizeof(float4x4));
	float4x4 world = BufferTable[2].Load<float4x4>(index.world_mat_index * sizeof(float4x4));

	float4x4 projection = BufferTable[2].Load<float4x4>(index.projection_mat_index);
	float4x4 model = BufferTable[2].Load<float4x4>(index.index);
	float4x4 world = BufferTable[2].Load<float4x4>(index.world_mat_index);
	*/

	float4x4 mod_to_world = mul(world,model);
	float4x4 clip = mul(projection, mod_to_world);
	float4 p = mul(clip,float4(vps,1.0f));
    OUT.world_vertex_p = mul(model,float4(vps,1.0f));
	OUT.position = p;
	OUT.frag_p = mul(model,float4(vps,1.0f));;    
	OUT.Color = float4(1,0,1,1);
	OUT.UV = uvps;
	OUT.normal = normalps;
	OUT.cam_p = float4(transpose(inverse(world))[3].xyz,0);
	OUT.world_mat_index = index.index;
	OUT.view_projection_index = 0;
	OUT.dummyo = 1;
	OUT.dummyt = 2;
	return OUT;
}
