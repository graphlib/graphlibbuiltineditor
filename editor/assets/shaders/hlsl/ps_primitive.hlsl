
struct PixelShaderInput
{
    float4 Position : SV_Position;
    float4 Color : COLOR0;
	float fog_factor : FOG;
	uint instance_id : SV_InstanceID;
	float4 TestColor : COLOR1;
};

float4 main( PixelShaderInput IN) : SV_Target0{
	float4 color = IN.Color;
	float4 fog_color = float4(0.3,0.3,0.3,0.3);
	float fog_start = 10;
	float fog_end = 20;
	float fog_factor = IN.fog_factor;
	fog_factor = distance(float3(0,0,0),IN.TestColor.rgb);
	float ff = (fog_end - fog_factor) / (fog_end - fog_start);
	ff = saturate(ff);

//color = float4(fog_factor,fog_factor,fog_factor,150/fog_factor);
//color = float4(IN.Color.rgb*(fog_factor/150),1);

	if (fog_factor > fog_end){
		color = float4(0,0,0,0);
	}

	color = lerp(fog_color * ff,color,ff);

	return color;
}

