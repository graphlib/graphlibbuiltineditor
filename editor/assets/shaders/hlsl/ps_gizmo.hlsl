struct PixelShaderInput
{
    float4 Position : SV_Position;
    float4 Color : COLOR0;
    float2 UV : TEXCOORD0;
	float3 normal : NORMAL;
	float4 frag_p : COLOR1;
	nointerpolation float4 cam_p : CUSTOMPOSITION;
	nointerpolation float world_mat_index : PSIZE;
	nointerpolation float view_projection_index : PSIZE1;
	nointerpolation float dummyo : PSIZE2;
	nointerpolation float dummyt : PSIZE3;
	nointerpolation float4 world_vertex_p : COLOR2;
	
};

struct IndexedStruct{
	uint index;
	uint mettalic_rougness_tex_index;
	uint normal_tex_index;
	uint emmisive_tex_index;
	float4 base_color;
	float metallic;
	float roughness;
	float emmisive;
	float dummy;
};

Texture2D ts[1000] : register(t10);
ConstantBuffer<IndexedStruct> index : register(b12);

float4 main( PixelShaderInput IN ) : SV_Target0{
	float4 color = index.base_color;
	return color;
}

