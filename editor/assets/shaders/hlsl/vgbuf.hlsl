#pragma pack_matrix( row_major )


struct MatrixStruct
{
    matrix m;
};

struct VertexPosColor
{
    float3 Position : POSITION;
    float3 normal : NORMAL;
    float2 UV : TEXCOORD;
};
 
struct VertexShaderOutput
{
    float4 position : SV_Position;    
    float4 Color : COLOR;
    float2 UV : TEXCOORD0;
    float3 normal : NORMAL;
};

StructuredBuffer<MatrixStruct> matrix_structured_buffer;

VertexShaderOutput main(VertexPosColor IN)
{
    VertexShaderOutput OUT;
 
    matrix world_mat;
    matrix clip_mat;
    float4 p = mul(float4(IN.Position,1.0f),clip_mat);
    //float4 world_p = mul(float4(IN.Position,1.0f),world_mat);
    
    OUT.position = p;
    OUT.Color = float4(1,1,1,1);
    OUT.UV = IN.UV;
    //NOTE(Ray):Assume normalized normal for now.
    OUT.normal = mul(IN.normal,world_mat);

    return OUT;
}
