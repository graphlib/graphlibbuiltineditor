struct PerDrawConstants
{
	uint vertexOffset;
    uint vertexBufferIndex;
    uint mat_id;
	uint world_mat_id;
};

struct VertexPosColor
{
    float3 Position : POSITION;
};

struct VertexShaderOutput
{
    float4 position : SV_Position0;    
    float4 Color : COLOR0;
	float fog_factor : FOG;
	uint instance_id : SV_InstanceID;
	float4 TestColor : COLOR1;
};

struct ShapeLine{
	float4 color;
	uint world_matrix_id;
};

struct IndexedStruct{
	uint index;
	uint world_mat_index;
	uint projection_mat_index;
	uint position_index;
    uint normal_index;
    uint uv_index;
    uint color_index;
};

ByteAddressBuffer BufferTable[1000] : register(t10);
StructuredBuffer<float4x4> matrix_buffer : register(t0);
StructuredBuffer<float3> line_buffer : register(t2);
ConstantBuffer<PerDrawConstants> draw_constants : register(b0);
ConstantBuffer<IndexedStruct> index : register(b11);
StructuredBuffer<ShapeLine> shape_line_buffer : register(t3);

VertexShaderOutput main(uint vertexID : SV_VertexID,uint instance_id : SV_InstanceID)
{
	VertexShaderOutput OUT;

	//TODO(Ray):Right now we are saying the projection matrix is always 0 
	//later we will get this from cameras that have a custom setting and 
	//set this to that through the PerDrawConstants
	//float4x4 projection = matrix_buffer[index.projection_mat_index];
    //float3 vps = asfloat(BufferTable[index.position_index].Load3(vertexID * 12));
    //float3 vps = asfloat(BufferTable[index.position_index].Load3(vertexID * 12));
    //float3 pvps = asfloat(BufferTable[2].Load3(vertexID * 12));
    //ShapeLine sl = line_buffer[vertexID];1
	//uint vid = vertexID % 2;
	uint vid = vertexID;
	uint iid = instance_id + 1;
	uint final_id;
	if (vid % 2 == 0){
		final_id = (2 * iid) - 2;
	}else{
		final_id = (2 * iid) - 1;
	}

    float3 pos = line_buffer[final_id];
    ShapeLine sl = shape_line_buffer[instance_id];
    
	//Fix the naming for these matrices
	float4x4 projection = matrix_buffer[index.projection_mat_index];
	float4x4 world = matrix_buffer[sl.world_matrix_id];
	float4x4 view = matrix_buffer[index.world_mat_index];

	float4x4 to_view = mul(view,world);
	float4x4 to_clip = mul(projection, to_view);
	float4 p = mul(to_clip,float4(pos,1.0f));
	OUT.position = p;
	OUT.Color = sl.color;
	OUT.instance_id = instance_id;

	float3 world_p = mul(world,float4(pos,1.0f)).xyz;
	float3 origin = float3(0,0,0);
	OUT.fog_factor = distance(world_p,origin);
	OUT.TestColor = float4(world_p,1.0);
	return OUT;
}
