package tools
import gfx "../../../../../graphlib/directx"
//import imgui "../../../../../graphlib/libs/odin-imgui"
import imgui "../../../../../graphlib/libs/imgui"
import "../../../../../graphlib/editor"
import "../../../../../graphlib/path"
import "../../../../../graphlib/engine"
import "../../../../../graphlib/globals"
//import json "../../../../../graphlib/internal/graphlibjson"

import "core:strings"
import m "core:math/linalg/hlsl"
import "core:fmt"

import "core:encoding/json"
import "core:os"

change_texture_mode : bool
change_texture_mode_index : int
selected_index : i32

material_editor_show :: proc(show_material_view : ^bool,eg : ^globals.EngineGlobals)-> (material_changed : bool){
	if imgui.Begin("Material View",show_material_view,{}){
		using gfx
		if editor.editor_globals.selected_node != nil{
			materials : [dynamic]^Material
			mesh_comp := scenenode_get_materials(editor.editor_globals.selected_node^,&materials)

			current_name : string

			if comp_ptr,ok := scenenode_has_component_ptr(editor.editor_globals.selected_node^,SpriteComponent);ok{
				imgui.Text(ctc(comp_ptr.material_uri))
			}

			//get list of all material from disk
			path_to_materials := path.path_create("assets://materials")
			handle,errno := os.open(path_to_materials.path,)
			file_infos,err := os.read_dir(handle,100,context.temp_allocator)
			fi_cs : [dynamic]cstring
			defer delete(fi_cs)

			for fi in file_infos{
				append(&fi_cs,ctc(fi.name))
			}
			//select in imgui combo 
			imgui.ComboChar("materials",&selected_index,raw_data(fi_cs[:]),i32(len(fi_cs)))
			//apply button
			if imgui.Button("Change Material"){
				sfi := file_infos[selected_index]
				if mat,ok := gfx.material_load_from_uri(sfi.fullpath);ok{
					mat.buffer_ids = materials[0].buffer_ids
					mat.index_count = materials[0].index_count
					mat.is_visible = materials[0].is_visible
					mat.vertex_count = materials[0].vertex_count
					mat.instances = materials[0].instances
					mat.matrix_id = materials[0].matrix_id
					mat.index_buffer_view = materials[0].index_buffer_view

					if gfx.materialinstance_remove_material(mesh_comp.material_instance_handles[0]){
					}
					pso := pipelinestateobject_create_from_material_with_default_desc(&mat)
					mat.pipeline_state_object_ref = pso
					mat_handle := material_add_to_global_material_list(mat)
					mat_instance_entry : MaterialInstanceEntry
					mat_instance_entry.material_handle = mat_handle
					instance_handle := materialinstance_add(mat_instance_entry)
					mesh_comp.material_instance_handles[0] = instance_handle//gfx.materialinstance_create_from_handle(material_handle)
					material_changed = true
				}else{
					engine.logger_add(fmt.tprintf("Could not load material from file info %s",sfi.name),.Error)
				}
			}

			//TODO(Ray):Show the properties in the material map 
			if materials != nil{
				for material in materials{
					mat_meta := editor.editor_create_rtti_info(material)
						//if strings.compare(di_info.name,"is_visible") == 0{
						//	is_visible : bool
						if imgui.Checkbox("is_visible",&material.is_visible){
						}
					//	}

					for di_info,di in mat_meta.fields{ 
						//imgui.push_id_int(i32(di))
						if strings.compare(di_info.name,"name") == 0{
							current_name = gfx.print_string(di_info.type_info,di_info.data,true,material_global_ptr.allocator)
						}

						if strings.compare(di_info.name,"properties") == 0{
							ok : bool
							depth_test : bool
							depth_test,ok = material.properties["depth_test"].prop.(bool)
							if imgui.Checkbox("depth_test",&depth_test){
								material.properties["depth_test"] = {"depth_test",depth_test}
							}
							depth_write : bool
							depth_write,ok = material.properties["depth_write"].prop.(bool)
							if imgui.Checkbox("depth_write",&depth_write){
								material.properties["depth_write"] = {"depth_write",depth_write}
							}

							double_sided : bool
							double_sided,ok = material.properties["double_sided"].prop.(bool)
							if imgui.Checkbox("double_sided",&double_sided){
								material.properties["double_sided"] = {"double_sided",double_sided}
							}

							if alpha_mode,aok := material.properties["alpha_mode"].prop.(Alpha_Mode);aok{
								names := gfx.get_enum_as_slice_with_typeid(alpha_mode)
								values := gfx.get_enum_as_slice_values_with_typeid(alpha_mode)
								input_combo := gfx.get_input_combo(alpha_mode,(rawptr)(&alpha_mode))

								cs : [dynamic]cstring
								defer delete(cs)
								for name in names{
									append(&cs,ctc(name))
								}

								if imgui.ComboChar("testmatcombo",&input_combo.selected_index,raw_data(cs[:]),i32(len(cs))){
									enum_value := values[input_combo.selected_index]
									material.properties["alpha_mode"] = {"alpha_mode",cast(Alpha_Mode)enum_value}
								}
							}

							imgui.Separator()

							imgui.Text("SHADER INPUTS")

							base_color_factor : m.float4
							if base_color_factor,ok = material.properties["base_color_factor"].prop.(m.float4);ok{
								gfx.print_f4(&base_color_factor,"base_color_factor")
								material.properties["base_color_factor"] = {"base_color_factor",base_color_factor}
							}

							{
								if metallic_factor,ok := material.properties["metallic_factor"].prop.(f32);ok{
									gfx.print_float(&metallic_factor,"metallic_factor")
									material.properties["metallic_factor"] = {"metallic_factor",metallic_factor}
								}

								if roughness_factor,ok := material.properties["roughness_factor"].prop.(f32);ok{
									gfx.print_float(&roughness_factor,"roughness_factor")
									material.properties["roughness_factor"] = {"roughness_factor",roughness_factor}
								}
							}

							//loop over properties find all properties that contain the key name texture this denotes a texture input to the shader
							i : int = 0
							imgui.Text(ctc(fmt.tprintf("change texture mode %v",change_texture_mode)))
							imgui.Text(ctc(fmt.tprintf("change texture index %v",change_texture_mode_index)))
							for k,v in material.properties{
								//fmt.println(k)
								imgui.PushIDInt(i32(i))
								if strings.contains(k,"texture"){
									imgui.Text(ctc(k))
									image_id : u64
									image_id,ok = v.prop.(u64)
									if ok{
										gpu_handle :=  graphics_get_gpu_handle_for_srv_heap(image_id)
										size := imgui.Vec2{100,100}
										imgui.Image(imgui.TextureID(uintptr(gpu_handle.ptr)),size)
									}

									if imgui.Button("change texture"){
										change_texture_mode = !change_texture_mode
										change_texture_mode_index = i
									}

									if change_texture_mode == true && eg.asset_browser_is_clicked && change_texture_mode_index == i{
										lt,key,ok := gfx.load_texture_from_file(eg.asset_browser_file_path)
										if ok{
											//em_ptr.descriptor.texture_uri = eg.asset_browser_file_path
											//em_ptr.descriptor.texture_id = lt.id
											material.properties[k] = {k,lt.id}
											if comp_ptr,ok := scenenode_has_component_ptr(editor.editor_globals.selected_node^,SpriteComponent);ok{
												comp_ptr.texture = lt
												if i == 0{
												}
												material.texture_ids[0] = int(lt.id)
												material.properties["color_texture_id"] = {"color_texture_id",lt.id}
												engine.logger_add(fmt.tprintf("wrote texture to SpriteComponent"),.Info)
											}
										}
									}
								}
								i = i + 1
								imgui.PopID()
							}

							if imgui.Button("apply material"){
								//TODO(Ray):delete old pipelinej state object
								pso := pipelinestateobject_create_from_material_with_default_desc(material)
								material.pipeline_state_object_ref = pso
							}

							imgui.Separator()

							if temp_vs_shader,ok := material.properties["vs_shader"].prop.(Shader);ok{
								imgui.Text(ctc(temp_vs_shader.meta_data.path))
							}

							if temp_ps_shader,ok := material.properties["ps_shader"].prop.(Shader);ok{
								imgui.Text(ctc(temp_ps_shader.meta_data.path))
							}

							if imgui.Button("save custom material for mesh"){
								//serializble_material : SerializableMaterial = {material.name}
								//temporarily remove the materials unmarshable properties and replace them with serizliable ones 
								//them reverse that process after teh serilzation is done.
								//for now that is the shader only atm
								if temp_vs_shader,ok := material.properties["vs_shader"].prop.(Shader);ok{
									if temp_ps_shader,ok := material.properties["ps_shader"].prop.(Shader);ok{
										serializable_shader_vs : SerializableShader = {temp_vs_shader.meta_data.path}
										serializable_shader_ps : SerializableShader = {temp_ps_shader.meta_data.path}

										delete_key(&material.properties,"vs_shader")
										delete_key(&material.properties,"ps_shader")

										temp_serial_properties := make(map[string]SerializableMaterialProperty)

										temp_serial_properties["vs_shader_serialized"] = {"vs_shader_serialized",serializable_shader_vs}
										temp_serial_properties["ps_shader_serialized"] = {"ps_shader_serialized",serializable_shader_ps}
										if tex_id,texok := material.properties["color_texture_id"].prop.(u64);texok{
											if color_texture,ok := gfx.texture_get_from_id(tex_id);ok{
												temp_serial_properties["color_texture_path"] = {"color_texture_path",color_texture.uri}
											}
										}
										
										//temp_serial_material["normal_texture_path"] = {"normal_texture_path",material.normal_texture_path}
										//temp_serial_material["mettalic_texture_path"] = {"metallic_texture_path",material.specular_texture_path}

										for k,v in material.properties{
											if k == "vs_shader_serialized"{
												continue
											}
											if k == "ps_shader_serialized"{
												continue
											}
											switch _ in v.prop{
												case u64:
												temp_serial_properties[k] = {v.name,v.prop.(u64)}
												case bool:
												temp_serial_properties[k] = {v.name,v.prop.(bool)}
												case m.float:
												temp_serial_properties[k] = {v.name,v.prop.(m.float)}
												case m.float3:
												temp_serial_properties[k] = {v.name,v.prop.(m.float3)}
												case m.float4:
												temp_serial_properties[k] = {v.name,v.prop.(m.float4)}
												case string:
												temp_serial_properties[k] = {v.name,v.prop.(string)}
												case Alpha_Mode:
												temp_serial_properties[k] = {v.name,v.prop.(Alpha_Mode)}
												case Shader:
												//temp_serial_properties[k] = {v.name,v.prop.(Shader)}
												case SerializableShader:
												temp_serial_properties[k] = {v.name,v.prop.(SerializableShader)}
											}
										}

										delete_key(&temp_serial_properties,"color_texture_id")

										serializble_material : SerializableMaterial = {current_name,temp_serial_properties}
										marshal_data,err := json.marshal(serializble_material)

										material.properties["vs_shader"] = {"vs_shader",temp_vs_shader}
										material.properties["ps_shader"] = {"ps_shader",temp_ps_shader}

										if err == nil{
											write_path := path.path_create(fmt.tprint("assets://materials/",material.name))
											os.write_entire_file(write_path.path,marshal_data)
											engine.logger_add(fmt.tprintf("JSON MARSHALED AND wrote material to material %s",write_path.path),.Error)
			//mesh_comp := scenenode_get_materials(editor.editor_globals.selected_node^,&materials)

											if comp_ptr,ok := scenenode_has_component_ptr(editor.editor_globals.selected_node^,SpriteComponent);ok{
												comp_ptr.material_uri = strings.clone(write_path.path)
												engine.logger_add(fmt.tprintf("wrote material_uri to SpriteComponent %s",write_path.path),.Info)
											}else{
												engine.logger_add("failed to get SpriteComponent",.Error)
											}
										}else{
											engine.logger_add("failed to marshal material",.Error)
										}
									}else{
										engine.logger_add("failed to get ps shader",.Error)
									}
								}else{
									engine.logger_add("failed to get vs shader",.Error)
								}//end of shader check
							}//end of imgui button for save material
						}
					}
				}
			}
			delete(materials)
		}
	}
	imgui.End()
	return material_changed
}

