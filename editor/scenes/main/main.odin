//TODO(Ray): Make all these tools separate packages for cleaniness not importatnt now i think.

package main
import "../../../../graphlib/globals"
import "../../../../graphlib/engine"
import "../../../../graphlib/engine/platform"
import "../../../../graphlib/engine/window"
import "../../../../graphlib/editor"
import gfx "../../../../graphlib/directx"
import con "../../../../graphlib/containers"
import confree "../../../../graphlib/containers/freelist"
import "core:runtime"
import "core:strings"
import "core:strconv"
import mem "core:mem"
import fmt "core:fmt"
import json "core:encoding/json"
//import json "../../../../graphlib/internal/graphlibjson"
import os "core:os"
import "core:bytes"
import "core:path/filepath"
import "core:intrinsics"
import la "core:math/linalg"
import m "core:math/linalg/hlsl"
import input "../../../../graphlib/engine/input"
import engine_path "../../../../graphlib/path"

import D3D12 "vendor:directx/d3d12"

//import imgui "../../../../graphlib/libs/odin-imgui"
import imgui "../../../../graphlib/libs/imgui"
import "core:c"

import "tools"

//conv fun
ctc :: proc(a : string)->(cstring){return strings.clone_to_cstring(a,context.temp_allocator)}

selected_node : ^gfx.SceneNode
is_show_draw_stats : bool
//marshal_arena : mem.Arena
//marshal_allocator : mem.Allocator

show_shader_editor : bool = false
show_console : bool = true
shader_editor_text : string
vs_shader_editor_text : string
s_ok : bool
shader_editor_text_file : []byte
editing_shader_node : ^gfx.SceneNode
editor_scene_tree : editor.EditorSceneTree
show_demo_window : bool = true
show_game_view : bool = false
package_editor_loaded : bool
material_changed : bool

shader_name := strings.clone("PS name")
vs_shader_name := strings.clone("VS name")


cbivs :: proc "c" (data: ^imgui.InputTextCallbackData) -> c.int{
	if data.EventFlag == {.CallbackEdit}{
		context = runtime.default_context()
		temp_string := strings.clone_from_cstring(data.Buf)
		//temp_string2 := (^string)(data.user_data)^
		vs_shader_editor_text = temp_string
	}
	return 0
}


cbi :: proc "c" (data: ^imgui.InputTextCallbackData) -> c.int{
	if data.EventFlag == {.CallbackEdit}{
		context = runtime.default_context()
		temp_string := strings.clone_from_cstring(data.Buf)
		//temp_string2 := (^string)(data.user_data)^
		shader_editor_text = temp_string
	}
	return 0
}

@export init :: proc(){
	eg := (^globals.EngineGlobals)(context.user_ptr)
	globals.init_globals(eg)
	engine.logger_add("editor init",.Info)
	//when #config(EDITOR,true){
		imgui.SetCurrentContext(gfx.imgui_global_ptr.ctx)
		//}
	}

	@export editor_update :: proc(dt : f32){
	eg := (^globals.EngineGlobals)(context.user_ptr)
	show_menu := true
	//editor.show_editor_menu(&show_menu)
	if imgui.BeginMainMenuBar(){
		if imgui.BeginMenu("File"){
			imgui.Separator()
			// TODO(Yuki): Make Alt+Q shortcut work
			if imgui.MenuItem("Exit"){
				platform.platform_global_ptr.ps.is_running = false
			}
			imgui.EndMenu()
		}

		if imgui.BeginMenu("Workspace"){
			if imgui.BeginMenu("Set Workspace Dir"){
				@static scene_file_name : []u8
				scene_file_name = make([]u8,100)
				@static scene_file_name_s : string
				save_callback :: proc "c" (data: ^imgui.InputTextCallbackData) -> c.int{
					context = runtime.default_context()
					scene_file_name_s = strings.clone_from_cstring(data.Buf)
					fmt.println(scene_file_name_s)
					return 0
				}

				sc := save_callback
				sd : imgui.InputTextCallbackData
				sd.UserData = &scene_file_name_s

				imgui.InputTextEx("workspacedir",cstring(&scene_file_name[0]),len(scene_file_name),{.CallbackEdit},sc,(rawptr)(&sd))

				if imgui.MenuItem("Set Directory"){
					//check if its a valid director
					//if ok than set it
					engine.subscene_global_ptr.game_workspace_dir = scene_file_name_s
					//and then save it to disk as json
					
					bytes,err := json.marshal(scene_file_name_s)
					os.write_entire_file("engineconfig.json",transmute([]u8)(bytes))
					engine.logger_add(fmt.tprintf("Set Workspace Dir to %s",engine.subscene_global_ptr.game_workspace_dir),.Info)
				}
				imgui.EndMenu()
			}//ends set workspace dir

			if imgui.BeginMenu("Scene"){
				@static scene_file_name : []u8
				scene_file_name = make([]u8,100)
				@static scene_file_name_s : string
				save_callback :: proc "c" (data: ^imgui.InputTextCallbackData) -> c.int{
					context = runtime.default_context()
					scene_file_name_s = strings.clone_from_cstring(data.Buf)
					fmt.println(scene_file_name_s)
					return 0
				}

				sc := save_callback
				sd : imgui.InputTextCallbackData
				sd.UserData = &scene_file_name_s

				imgui.InputTextEx("scene name",cstring(&scene_file_name[0]),len(scene_file_name),{.CallbackEdit},sc,&sd)

				if imgui.MenuItem("scene create"){
					using fmt
					println(scene_file_name_s)
					final_string := scene_file_name_s
					final_string = strings.trim_null(final_string)
					global_scene_path := "scenes/"
					global_asset_scene_path := "assets/scenes/"
					final_dir_string := filepath.join([]string{global_scene_path,final_string})
					json_final_path_string := filepath.join([]string{engine.subscene_global_ptr.game_workspace_dir,global_asset_scene_path})
					json_file_name := strings.concatenate([]string{final_string,".json"})
					json_final_path := filepath.join([]string{json_final_path_string,json_file_name})
					//final_string = filepath.join([]string{game_workspace_dir,final_string,".json"})
					final_dir_string = filepath.join([]string{engine.subscene_global_ptr.game_workspace_dir,final_dir_string})

					engine.logger_add(fmt.tprintf("Creating director for scene at %v",final_dir_string),.Info)
					os.make_directory(final_dir_string,u32(os.File_Mode_Dir))

					final_dir_string_main := filepath.join([]string{final_dir_string,"main.odin"})

					template_string := `
					package %v
					import "../../../../graphlib/globals"

					@export init :: proc(){{
						eg := (^globals.EngineGlobals)(context.user_ptr)
						globals.init_globals(eg)
					}}

					@export update :: proc(){{
					}}
					`

					string_to_write : string = fmt.tprintf(template_string,scene_file_name_s) 
					os.write_entire_file(final_dir_string_main,transmute([]u8)(string_to_write))

					json_string_to_write := 
					`
					{{
						"name": "%v",
						"dylib_id": 0,
						"nodes": {{
							"buffer": [
							{{
								"name": "Root",
								"transform": {{
									"p": [
									0.00000000,
									0.00000000,
									0.00000000
									],
									"r": [
									0.0000000000000000,
									0.0000000000000000,
									0.0000000000000000,
									1.0000000000000000
									],
									"s": [
									1.00000000,
									1.00000000,
									1.00000000
									],
									"local_p": [
									0.00000000,
									0.00000000,
									0.00000000
									],
									"local_r": [
									0.0000000000000000,
									0.0000000000000000,
									0.0000000000000000,
									1.0000000000000000
									],
									"local_s": [
									1.00000000,
									1.00000000,
									1.00000000
									],
									"forward": [
									0.00000000,
									0.00000000,
									1.00000000
									],
									"up": [
									0.00000000,
									1.00000000,
									0.00000000
									],
									"right": [
									1.00000000,
									0.00000000,
									0.00000000
									]
								}},
								"children": {{
									"buffer": [],
									"current_id": 0,
									"borrow_count": 0,
									"is_init": true
								}},
								"asset_uri": "",
								"is_prefab": false,
								"is_drawable_sprite": false,
								"component_ids": []
							}}
							],
							"current_id": 0,
							"borrow_count": 0,
							"is_init": true
						}}
					}}`

					json_string_to_write = fmt.tprintf(json_string_to_write,scene_file_name_s)
					os.write_entire_file(json_final_path,transmute([]u8)(json_string_to_write))

					json_file_name_spr := strings.concatenate([]string{final_string,".json.sprites"})
					json_final_path_spr := filepath.join([]string{json_final_path_string,json_file_name_spr})

					json_string_to_write = `{"name": "SpriteComponentSerialized", "array": []}`
					os.write_entire_file(json_final_path_spr,transmute([]u8)(json_string_to_write))

					json_file_name_cam := strings.concatenate([]string{final_string,".json.cameras"})
					json_final_path_cam := filepath.join([]string{json_final_path_string,json_file_name_cam})

					json_string_to_write = `{"name": "CameraComponentSerialized", "array": []}`
					os.write_entire_file(json_final_path_cam ,transmute([]u8)(json_string_to_write))
					//in the gameworkspace scenes dir
				}

				imgui.EndMenu()
			}//ends scene
			imgui.EndMenu()
		}//ends workspace

		if imgui.BeginMenu("Load"){
			if imgui.BeginMenu("files"){
				//workspace path
				sp := strings.clone(engine.subscene_global_ptr.game_workspace_dir)
				np := "assets/scenes/"
				scenes_path := filepath.join([]string{sp,np})

				//engine.logger_add(fmt.tprintf("loading from scenes path %v",scenes_path),.Info)
				asset_fd,err := os.open(scenes_path,os.O_RDONLY,0)
				fis,ferr := os.read_dir(asset_fd,0)
				for f in fis{
					if !f.is_dir{
						if imgui.MenuItem(ctc(f.name)){
							selected_node = nil
							gfx.scene_load(engine.subscene_global_ptr.game_workspace_dir,f.name)
							//For now this is done in the graphlib.odin
							/*
							if package_editor_loaded == false{
								gfx.scene_load(engine.subscene_global_ptr.game_workspace_dir,"editor_packages.json",true)
								package_editor_loaded = true
							}
							*/
						}
					}
				}
				imgui.EndMenu()
			}

				/*
			if imgui.menu_item("BUILDLoad"){
				if imgui.begin_menu("files"){
					//workspace path
					sp := strings.clone(engine.subscene_global_ptr.game_workspace_dir)
					np := "assets/scenes/"
					scenes_path := filepath.join([]string{sp,np})

					//engine.logger_add(fmt.tprintf("loading from scenes path %v",scenes_path),.Info)
					asset_fd,err := os.open(scenes_path,os.O_RDONLY,0)
					fis,ferr := os.read_dir(asset_fd,0)
					for f in fis{
						if !f.is_dir{
							if imgui.menu_item(f.name){
								selected_node = nil
								gfx.scene_load(engine.subscene_global_ptr.game_workspace_dir,f.name)
							}
						}
					}
					imgui.end_menu()
				}
			}
				*/

			global_scene_path : string = "assets/scenes/"
			if imgui.MenuItem("SaveLoad"){
				final_string := strings.concatenate([]string{global_scene_path,gfx.scene_global_ptr.current_scene_file_name,".json"})
				editor.scene_save(engine.subscene_global_ptr.game_workspace_dir,final_string)
				scene_file_name := strings.concatenate([]string{gfx.scene_global_ptr.current_scene_file_name,".json"})
				gfx.scene_load(engine.subscene_global_ptr.game_workspace_dir,scene_file_name)
			}

			if imgui.MenuItem("Save"){
				global_asset_scene_path := "assets/scenes/"
				name_string := strings.concatenate([]string{gfx.scene_global_ptr.current_scene_file_name,".json"})
				//final_string := filepath.join([]string{game_workspace_dir,global_scene_path,name_string})
				editor.scene_save(engine.subscene_global_ptr.game_workspace_dir,name_string)
			}

			using imgui
			if BeginMenu("save as"){
				@static file_name : []u8
				file_name = make([]u8,100)
				@static file_name_s : string
				save_callback :: proc "c" (data: ^imgui.InputTextCallbackData) -> c.int{
					context = runtime.default_context()
					file_name_s = strings.clone_from_cstring(data.Buf)
					fmt.println(file_name_s)
					return 0
				}

				sc := save_callback
				sd : imgui.InputTextCallbackData
				sd.UserData = &file_name_s

				imgui.InputTextEx("filename",cstring(&file_name[0]),len(file_name),{.CallbackEdit},sc,&sd)

				if MenuItem("save"){
					using fmt
					println(file_name_s)
					final_string := file_name_s
					final_string = strings.trim_null(final_string)
					final_string = strings.concatenate([]string{global_scene_path,final_string,".json"})
					editor.scene_save(engine.subscene_global_ptr.game_workspace_dir,final_string)
				}
				EndMenu()
			}
			imgui.Separator()
			imgui.EndMenu()
		}//ends load

		if imgui.BeginMenu("Gizmo Settings"){
			using gfx.gizmo_global_ptr
			if imgui.BeginMenu("Space Mode"){
				if imgui.MenuItem("Local Space"){
					gizmo_space_mode = .Local
				}
				if imgui.MenuItem("World Space"){
					gizmo_space_mode = .World
					gizmo_rn.transform.r = la.QUATERNIONF32_IDENTITY
				}
				imgui.EndMenu()
			}
			imgui.EndMenu()
		}

		if imgui.BeginMenu("Package Manager"){
			if imgui.MenuItem("Register workspace package"){
				//get a list of packages from the workspace

			}
			if imgui.MenuItem("Download package to workspace"){
				//download a package from the internet
				//auto registere it
			}
			if imgui.MenuItem("Download package from package workspace template?"){
				//download a set of packages using another space from another packages
			}
			if imgui.MenuItem("Copy packages from workspace to another workspace?"){
				//download a set of packages using another space from another packages
			}
			if imgui.MenuItem("Register package to workspace"){
				//Add the package to the package editor list
				//add the procs calls to the right place
				//get and show list of packages
				//EDITOR packages and RUNTIME packages might be different
				wspace_path := engine.subscene_global_ptr.game_workspace_dir
				fpath := filepath.join([]string{wspace_path, "packages/package_info.json"})
				//file_bytes,ferr := os.open(fpath,os.O_RDONLY,0)
				file_bytes,ferr := os.read_entire_file(fpath)

				Package_Struct_Element :: struct{
					name : string,
					dir : string,
				}

				Package_Struct :: struct{
					editor : [dynamic]Package_Struct_Element,
				}

				check_json_unmarshal_error :: proc(err : json.Unmarshal_Error)-> bool{
					#partial switch v in err{
						case json.Error:
						case json.Unmarshal_Data_Error:
						fmt.println("package register:error unmarshalling data")
						return true
						case json.Unsupported_Type_Error:
						fmt.println("package register:unsupported type error")
						return true
					}
					return false
				}

				ps := Package_Struct{}
				err := json.unmarshal((file_bytes),&ps)
				if check_json_unmarshal_error(err){
					fmt.println("Could not unmarshal package.json in register package to workspace")
				}
			}
			imgui.EndMenu()
		}

		if imgui.MenuItem("Save scene for auto run on startup"){
		}

		/*
		if imgui.BeginMenu("DebugWindows"){
			if imgui.MenuItem("Draw Stats"){
				//is_show_draw_stats = !is_show_draw_stats
			}
			imgui.EndMenu()
		}
		*/

		imgui.EndMainMenuBar()
	}

	if input.input_global_ptr.keyboard.keys[input.input_global_ptr.keys.f1].released{
		selected_node = nil
		scene_file_name := strings.concatenate([]string{gfx.scene_global_ptr.current_scene_file_name,".json"})
		gfx.scene_load(engine.subscene_global_ptr.game_workspace_dir,scene_file_name)
	}

	if input.input_global_ptr.keyboard.keys[input.input_global_ptr.keys.f2].down{
		//scene_file_name := "main"//strings.concatenate([]string{gfx.current_scene_file_name,".json"})
		dir := "../graphlibeditor/editor/"
		gfx.scene_load(dir,"main.json",false,true)
		engine.logger_add("Reloading builtin editor",.Info)
	}

	if input.input_global_ptr.keyboard.keys[input.input_global_ptr.keys.f3].released{
		selected_node = nil
		scene_file_name := strings.concatenate([]string{gfx.scene_global_ptr.current_scene_file_name,".json"})
		gfx.scene_load(engine.subscene_global_ptr.game_workspace_dir,scene_file_name,false,false,true)
	}

	is_open := true

	if !imgui.Begin("Controls",&is_open,{}){
	}

	imgui.Text("current workspace: %s",engine.subscene_global_ptr.game_workspace_dir)

	using gfx.scene_global_ptr
	if imgui.Button("pause/resume"){
		is_game_bridge_running = !is_game_bridge_running
	}
	if is_game_bridge_running == false{
		platform.platform_global_ptr.ps.time.delta_seconds = 0
	}

	imgui.End()//controls

	//Nothing shown to you select an object than we get the material and shader and when you press
	//the shader editor button the original pso is destroyed and a new one is created with the new shader
	//and attached to the MeshComponent
	//there may be cases where you want to have a new material and branch off instead of overwriting all the 
	//the instances of a particular shader.
	if imgui.Begin("Shader Editor",&show_shader_editor,{}){
		using gfx
		flags : imgui.InputTextFlags
		flags = {.CallbackResize,.CallbackEdit,.CallbackCompletion}

		cb : imgui.InputTextCallback = cbi
		cbvs : imgui.InputTextCallback = cbivs
		cbd : imgui.InputTextCallbackData

		if (editor.editor_globals.selected_node != nil && editor.editor_globals.selected_node != editing_shader_node) ||
			(material_changed == true){
			editing_shader_node = editor.editor_globals.selected_node
			materials : [dynamic]^gfx.Material
			mesh_comp := scenenode_get_materials(editor.editor_globals.selected_node^,&materials)
			if len(materials) > 0{
				material := materials[0]
				shader,shader_ok := material.properties["ps_shader"].prop.(Shader)
				if shader_ok{
					shader_editor_text = shader.meta_data.source
				}

				vsshader,vsshader_ok := material.properties["vs_shader"].prop.(Shader)
				if vsshader_ok{
					vs_shader_editor_text = vsshader.meta_data.source
				}
			}
			delete(materials)
		}

		if editing_shader_node != nil{
			//imgui.push_id_str("vs")
			sp := &vs_shader_editor_text
			cbd.UserData = &sp
			imgui.InputTextMultilineEx("Name of vs shader",ctc(vs_shader_editor_text),len(vs_shader_editor_text)*2,imgui.Vec2{1000,1000},flags,cbivs,&cbd)
			if imgui.Button("reload vs"){
				materials : [dynamic]^Material
				mesh_comp := scenenode_get_materials(editor.editor_globals.selected_node^,&materials)
				for material in materials{
					new_shader,is_compile_ok := compile_shader(vs_shader_editor_text,"vs_6_6")
					if is_compile_ok == true{
						material.properties["vs_shader"] = {"vs_shader",new_shader}
						pso := pipelinestateobject_create_from_material_with_default_desc(material)
						material.pipeline_state_object_ref = pso
						//TODO(Ray):delete pso
					}else{
						engine.logger_add("Compile of VertexShader failed",.Error)
					}
				}
				delete(materials)
			}

			imgui.Text("VertexShader:")
			imgui.SameLine()
			vs_shader_name_local := gfx.print_string(nil,(rawptr)(&vs_shader_name),true)

			if imgui.Button("save vs as"){
				//get the shader path
				materials : [dynamic]^Material
				mesh_comp := scenenode_get_materials(editor.editor_globals.selected_node^,&materials)
				for material in materials{
					//overwrite the file with current shater multitext content
					shader,is_ok := material.properties["vs_shader"].prop.(Shader)

					final_path := engine_path.path_create("assets://shaders")
					file_path := filepath.join([]string{final_path.path,vs_shader_name})
					shader.meta_data.path = file_path
					shader.meta_data.source = vs_shader_editor_text

					if !is_ok{

						engine.logger_add("No VS shader existed created a new one",.Info)
					}

					material.properties["vs_shader"] = {"vs_shader",shader} 

					if write_ok := os.write_entire_file(file_path,transmute([]byte)(vs_shader_editor_text));write_ok{
						engine.logger_add(fmt.tprintf("Wrote shader file to path %s",file_path),.Info)
					}else{
						engine.logger_add(fmt.tprintf("Error: Writing file to path %s",file_path),.Info)
					}
				}
			}

			if imgui.Button("save vs"){
				//get the shader path
				materials : [dynamic]^Material
				mesh_comp := scenenode_get_materials(editor.editor_globals.selected_node^,&materials)
				for material in materials{
					//new_shader,is_compile_ok := compile_shader(shader_editor_text,"ps_6_6")
					//vs_path := new_shader.meta_data.path
					//overwrite the file with current shater multitext content
					shader := material.properties["vs_shader"].prop.(Shader)
					if write_ok := os.write_entire_file(shader.meta_data.path,transmute([]byte)(vs_shader_editor_text));write_ok{
						engine.logger_add(fmt.tprintf("Wrote shader file to path %s",shader.meta_data.path),.Info)
					}else{
						engine.logger_add(fmt.tprintf("Error: Writing file to path %s",shader.meta_data.path),.Info)
					}
				}
			}

			sp = &shader_editor_text
			cbd.UserData = &sp
			imgui.InputTextMultilineEx("Name of ps shader",ctc(shader_editor_text),len(shader_editor_text)*2,imgui.Vec2{1000,1000},flags,cbi,&cbd)
			if imgui.Button("reload ps"){
				materials : [dynamic]^Material
				mesh_comp := scenenode_get_materials(editor.editor_globals.selected_node^,&materials)
				for material in materials{
					new_shader,is_compile_ok := compile_shader(shader_editor_text,"ps_6_6")
					if is_compile_ok == true{
						old_shader := material.properties["ps_shader"].prop.(Shader)
						new_shader.meta_data = old_shader.meta_data
						material.properties["ps_shader"] = {"ps_shader",new_shader}
						pso := pipelinestateobject_create_from_material_with_default_desc(material)
						material.pipeline_state_object_ref = pso
						//TODO(Ray):delete pso
					}else{
					}
				}
				delete(materials)
			}

			imgui.Text("PS Shader:")
			imgui.SameLine()
			shader_name_local := gfx.print_string(nil,(rawptr)(&shader_name),true)

			if imgui.Button("save ps as"){
				//get the shader path
				materials : [dynamic]^Material
				mesh_comp := scenenode_get_materials(editor.editor_globals.selected_node^,&materials)
				for material in materials{
					//overwrite the file with current shater multitext content
					shader,is_ok := material.properties["ps_shader"].prop.(Shader)
					
					file_path_dir,file := filepath.split(shader.meta_data.path)
					final_path := engine_path.path_create("assets://shaders")
					file_path := filepath.join([]string{final_path.path,shader_name})

					shader.meta_data.path = file_path
					shader.meta_data.source = shader_editor_text

					material.properties["ps_shader"] = {"ps_shader",shader} 

					if write_ok := os.write_entire_file(file_path,transmute([]byte)(shader_editor_text));write_ok{
						engine.logger_add(fmt.tprintf("Wrote shader file to path %s",file_path),.Info)
					}else{
						engine.logger_add(fmt.tprintf("Error: Writing file to path %s",file_path),.Info)
					}
				}
			}

			if imgui.Button("save ps"){
				//get the shader path
				materials : [dynamic]^Material
				mesh_comp := scenenode_get_materials(editor.editor_globals.selected_node^,&materials)
				for material in materials{
					//new_shader,is_compile_ok := compile_shader(shader_editor_text,"ps_6_6")
					//vs_path := new_shader.meta_data.path
					//overwrite the file with current shater multitext content
					shader := material.properties["ps_shader"].prop.(Shader)
					if write_ok := os.write_entire_file(shader.meta_data.path,transmute([]byte)(shader_editor_text));write_ok{
						engine.logger_add(fmt.tprintf("Wrote shader file to path %s",shader.meta_data.path),.Info)
					}else{
						engine.logger_add(fmt.tprintf("Error: Writing file to path %s",shader.meta_data.path),.Info)
					}
				}
			}
		}
	}
	imgui.End()//end shader editor
	
	if imgui.Begin("Console Log",&show_console,{}){

		for log in engine.logger_global_ptr.error_logs_backing{
			v4 : imgui.Vec4 = imgui.Vec4{log.color.x,log.color.y,log.color.z,log.color.w}
			imgui.TextColored(v4,ctc(log.message))
		}

		/*
		for shader_err in gfx.shader_error_logs{
			//imgui.text_colored(imgui.Vec4{1,0,0,1},shader_err)
			imgui.text(shader_err)
		}
		*/
	}
	imgui.End()//console log

	imgui.SetNextWindowSize(imgui.Vec2{520,600},imgui.Cond.FirstUseEver)
	imgui.Begin("Scene Objects",&editor_scene_tree.is_showing,{})

	if imgui.Button("Add Scene Node"){
		using gfx
		using scene_global_ptr
		new_so : SceneNode
		new_so.name = fmt.aprintf("New_Scene_Node_%d",con.buf_len(scene_nodes))
		new_m : m.float4x4
		new_so.transform,new_m = transform_init()
		new_so.transform.s = {1,1,1}
		new_so.transform.r = la.QUATERNIONF32_IDENTITY
		//new_so.component_key = ComponentKey(-1)
		new_so.is_prefab = false
		{
			using data_global_ptr
			context.allocator = global_matrix_buffer_allocator
			new_so.m_id = confree.buf_push(&global_matrix_buffer_cpu,new_m)
		}
		scenenode_push_to_root(new_so,true)
	}
		/*
		if imgui.Button("test Add Camera NOde "){
			using scene_global_ptr
			new_so : SceneNode
			new_so.name = fmt.aprintf("New Camera Node %d",con.buf_len(scene_nodes))
			new_m : m.float4x4
			new_so.transform,new_m = transform_init()
			new_so.transform.s = {1,1,1}
			new_so.transform.r = la.QUATERNIONF32_IDENTITY
			//new_so.component_key = ComponentKey(-1)
			new_so.is_prefab = false
			{
				using data_global_ptr
				context.allocator = global_matrix_buffer_allocator
				new_so.m_id = confree.buf_push(&global_matrix_buffer_cpu,new_m)
			}

			id := camera_system_add_camera(new_so.transform,.perspective)

			cam_comp : CameraComponent
			cam_comp.cam_id = id 
			append(&new_so.components,cam_comp)
			
			path := strings.concatenate([]string{editor_asset_cwd,"textures/","camera.png"})
			new_handle := scenenode_push_to_root(new_so,true)
			//sprite_add_to_scene(path,new_so.transform,false)
			sprite_add_to_scenenode(path,new_so.transform,new_handle,false)
		}
		*/
		/*
		if imgui.Button("test Add Sprite Scene NOde "){
			sprite_add_to_scene(asset_cwd)
		}
		*/

		if imgui.Button("Delete Selected Node"){
			using gfx
			if editor.editor_globals.selected_node != nil{
				{
					if editor.editor_globals.selected_node.is_drawable_sprite{
						//TODO(Ray): Delete sprite group and layers 
						//ask user if he is sure he want to delete his sprite edited
					}
					context.allocator = geometryloader_global_ptr.render_node_allocator
					scenenode_delete_node(editor.editor_globals.selected_node)
					//scenenode_delete(editor.selected_node^)
					editor.editor_globals.selected_node = nil

					//for _,leak in render.test_tracking_allocator.allocation_map{
					//	fmt.printf("%v leaked %v bytes\n", leak.location, leak.size)
					//}
					//for bad_free in render.test_tracking_allocator.bad_free_array{
					//	fmt.printf("%v allocation %p was freed badly\n", bad_free.location, bad_free.memory)
					//}
				}
			}
		}

		editor.fmj_editor_scene_tree_show(&editor_scene_tree,gfx.scenenode_get_root_handle())
		imgui.End()

		//Load and store images when we live the diretory delete the textures from the texture cache and gpu cache
		//Texilization will now be a part of this projects editor and we can call it to iterate over 
		//a director iteratively and convert all images in place to power of 2 textures
		//We should have a mirror directory for the converted images 
		//or to better simulate a an actual pipeline source assets are stored in a off workspace directory.
		//Texilzer can be run to import those assets into the workspace director mirroring the directory structure.
		//Should be able to specify multiple directories and get teh files recursively.
		//eventually we would want a directory watcher to watch for changes in teh source file...
		//auto convert and than replace instance of that converted texture in the cache with the new 
		//data.
		if imgui.Begin("Asset View",&show_game_view,{}){
			using gfx
			if eg.asset_browser_is_clicked == true{
				//seems like a leek but crashes wehn I delte why? moving on for now
				//delete(engine_globals.asset_browser_file_path)
				eg.asset_browser_is_clicked = false
			}
			if imgui.Button("../"){
				parent_directory := filepath.dir(asset_cwd)
				delete(asset_cwd)
				asset_cwd = parent_directory
			}

			asset_fd,err := os.open(asset_cwd,os.O_RDONLY,0)
			defer os.close(asset_fd)
			fis,ferr := os.read_dir(asset_fd,0,context.temp_allocator)

			cumalative_width : f32 = 0.0
			last_cursor_pos : imgui.Vec2
			last_image_width : f32
			last_cursor_pos = imgui.GetCursorPos()
			for file in fis {
				//is a directory
				ext := filepath.ext(file.name)
				size := imgui.Vec2{100,100}
				if file.is_dir{
					if imgui.Button(ctc(file.name)){
						child_directory := filepath.join({asset_cwd, file.name})
						delete(asset_cwd)
						asset_cwd = child_directory
					}
				}else{
					if strings.compare(ext,".png") == 0{
						tex,k,is_ok := load_texture_from_file(filepath.join({asset_cwd, file.name}))
						gpu_handle :=  graphics_get_gpu_handle_for_srv_heap(tex.id)
						tex_ar := (tex.dim.x)/(tex.dim.y)

						tex_view_scale : f32 = 64.0
						tex_view_dim := m.float2{1 * tex_ar,1} * tex_view_scale 
						size = imgui.Vec2{tex_view_dim.x,tex_view_dim.y}
						last_cursor_pos = imgui.GetCursorPos()
						if imgui.ImageButton(ctc(file.name),imgui.TextureID(uintptr(gpu_handle.ptr)),size){
							eg.asset_browser_is_clicked = true
							eg.asset_browser_file_path = filepath.join({asset_cwd, file.name})
						}
						//NOTE(Ray):This also causes llvm error in odin right now.
						//imgui.text_wrapped
					}else if strings.compare(ext,".gltf") == 0{
						if imgui.ButtonEx(ctc(file.name),size){
							new_handle := geometryloader_read_model(.disk, filepath.join({asset_cwd, file.name}, context.temp_allocator))
							model_ref_ptr := scenenode_get_reference(new_handle)
							scenenode_push_to_root(model_ref_ptr^)
							scenenode_update_nodes(model_ref_ptr)
						}
					}else{//other files
						if imgui.ButtonEx(ctc(file.name),size){
							eg.asset_browser_is_clicked = true
							eg.asset_browser_file_path = filepath.join({asset_cwd, file.name})
						}
					}
				}

				cumalative_width += size.x

				ww := imgui.GetContentRegionAvail()
				//TODO:needs improvement.
				if ww.x < cumalative_width + 300{
					cumalative_width = 0
				}else{
					last_cursor_pos.y = last_cursor_pos.y + size.y
					last_cursor_pos.x += size.x
					imgui.SetCursorPos(last_cursor_pos)
					imgui.SameLine()
				}
			}

			free_all(context.temp_allocator)
		}
		imgui.End()

		if imgui.Begin("Properties View",&show_game_view,{}){
			if editor.editor_globals.selected_node != nil{
				if editor.print_so(editor.editor_globals.selected_node) == true{
					gfx.scenenode_update_nodes(editor.editor_globals.selected_node)
				}
			}
		}
		imgui.End()

		show_material_view : bool = true
		material_changed = tools.material_editor_show(&show_material_view,eg)

		//editor.show_draw_stats_window()
		using gfx.sceneviews_global_ptr
		for sv in &scene_views.buffer{
			using gfx
			is_game_view := (strings.compare(sv.name,"game_view") == 0)

			flags : imgui.WindowFlag
			if sv.is_hovered{
				flags |= .NoMove
			}
			flags |= .MenuBar

			if imgui.Begin(ctc(sv.name),&sv.is_open,{flags}){
				if imgui.BeginMenuBar(){
					if imgui.BeginMenu("Camera"){
						if imgui.BeginMenu("projection type"){
							if imgui.MenuItem("Orthographic"){
								mod_cam := camera_get_ptr(sv.camera_id)
								mod_cam.projection_type = .orthographic
								window_dim := window.get_client_window_dim()
								mod_cam.projection_matrix = init_ortho_proj_matrix(window_dim,5,1,100)

								using data_global_ptr
								rc_mat := buffer_get_pointer_at_offset(&global_matrix_buffer,mod_cam.projection_matrix_id)
								rc_mat^ = mod_cam.projection_matrix

							}
							if imgui.MenuItem("Perspective"){
								//sv.camera.projection_type = Camera_Projection_Type.Orthographic
								mod_cam := camera_get_ptr(sv.camera_id)
								mod_cam.projection_type = .perspective
								window_dim := window.get_client_window_dim()
								mod_cam.projection_matrix = init_pers_proj_matrix(window_dim, mod_cam.fov, mod_cam.near_far_planes);

								using data_global_ptr
								rc_mat := buffer_get_pointer_at_offset(&global_matrix_buffer,mod_cam.projection_matrix_id)
								rc_mat^ = mod_cam.projection_matrix
							}

							imgui.EndMenu()
						}
						imgui.EndMenu()
					}
					
					if imgui.BeginMenu("Show Debug"){
						if imgui.MenuItem("lines off/on"){
							mat := material_get_from_global_material_list(primitivedraw_global_ptr.mat_handle)
							mat.is_visible = !mat.is_visible
							sv.settings.show_debug_lines = mat.is_visible
						}
						if imgui.MenuItem("gizmos off/on"){
							sv.settings.show_gizmos = !sv.settings.show_gizmos
							scenenode_is_visible_and_children(gizmo_global_ptr.gizmo_rn,sv.settings.show_gizmos)
						}
						imgui.EndMenu()
					}
					imgui.EndMenuBar()
				}


				//TOOD(Ray):Later on we might want to get the use a choice for what aspect ratio to 
				//view it at.
				small_size := imgui.GetWindowSize()
				sv.view_dim = m.float2{small_size.x,small_size.y}
				wd := window.get_client_window_dim()
				sv.window_dim = m.float2{wd.x,wd.y}
				wdar := wd.x / wd.y
				small_size.y = small_size.x / wdar

				wp := imgui.GetWindowPos()
				sv.window_pos = m.float2{wp.x,wp.y}
				sv.image_size = m.float2{small_size.x,small_size.y}

				camera := camera_get(sv.camera_id)
				srv_gpu_handle : D3D12.GPU_DESCRIPTOR_HANDLE = camera.viewport.render_target.srv_gpu_handle
				cursor_pos : imgui.Vec2
				cursor_pos = imgui.GetCursorScreenPos()
				sv.image_pos = m.float2{cursor_pos.x,cursor_pos.y}

				imgui.Image(imgui.TextureID(uintptr(srv_gpu_handle.ptr)),small_size)

				if imgui.IsItemHovered({}) && is_game_view == false{
					sv.is_hovered = true
					//if imgui.is_mouse_down(imgui.Mouse_Button.Left) && input.keyboard.keys[input.keys.alt_l].down{
					using input.input_global_ptr
					if keyboard.keys[keys.alt_l].down{
						camera_free(sv.camera_id,platform.platform_global_ptr.ps.time.delta_seconds)
					}
					if imgui.IsMouseReleased(imgui.MouseButton.Left) && gfx.gizmo_global_ptr.start_gizmo_drag == false{
						editor.editor_globals.selected_node = scene_get_closest_screen_cast(sv)
					}
				}else{
					sv.is_hovered = false
				}
			}
			imgui.End()
			//order this is called matters relative to a few other things
			gizmos_update(editor.editor_globals.selected_node)
		}
}

@export update :: proc(dt : f32){
	eg := (^globals.EngineGlobals)(context.user_ptr)
}


